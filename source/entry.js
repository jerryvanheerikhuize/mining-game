// import { HBDataServer } from './HBDataServer/HBDataServer.js';

import { game } from './configs/game.js';
import { tiles } from './mappers/tiles.js';
import { worlds } from './models/worlds.js';
import { life } from './models/life.js';
import { renderViewport, renderWorld, renderLife } from './renderers/renderers.js';
import { wakeDaemons } from './engine/daemons.js';

export function init(worldStr, parentDOM, worlds, game, tiles) {
    renderViewport(parentDOM, game);
    renderWorld(worldStr, worlds[worldStr], parentDOM, game, tiles);
    renderLife('player', life['player'], parentDOM, game);
    wakeDaemons();

    // const dataServer = new HBDataServer({key:'val'});

    return true;
}

init('earth', document.getElementById('viewport'), worlds, game, tiles);
