import { game } from './game.js';
import { expect } from 'chai';

describe('configs/game.js', () => {
    // Prevents additions and deletions
    it('expects the length of game to equal 3', () => {
        expect(Object.keys(game).length).to.equal(3);
    });

    it('expects the length of game.classes to equal 12', () => {
        expect(Object.keys(game.classes).length).to.equal(12);
    });

    it('expects the length of game.sizes to equal 3', () => {
        expect(Object.keys(game.sizes).length).to.equal(3);
    });

    // Check the types
    it('expects the type of game to be an object', () => {
        expect(game).to.be.a('object');
    });

    it('expects the type of game.viewport to be a sting', () => {
        expect(game.viewport).to.be.a('string');
    });

    it('expects the type of game.classes to be an object', () => {
        expect(game.classes).to.be.a('object');
    });
    it('expects the type of game.classes.world to be a string', () => {
        expect(game.classes.world).to.be.a('string');
    });
    it('expects the type of game.classes.backgroundcolor to be a string', () => {
        expect(game.classes.backgroundcolor).to.be.a('string');
    });
    it('expects the type of game.classes.background to be a string', () => {
        expect(game.classes.background).to.be.a('string');
    });
    it('expects the type of game.classes.walls to be a string', () => {
        expect(game.classes.walls).to.be.a('string');
    });
    it('expects the type of game.classes.blocks to be a string', () => {
        expect(game.classes.blocks).to.be.a('string');
    });
    it('expects the type of game.classes.objects to be a string', () => {
        expect(game.classes.objects).to.be.a('string');
    });
    it('expects the type of game.classes.player to be a string', () => {
        expect(game.classes.player).to.be.a('string');
    });
    it('expects the type of game.classes.foreground to be a string', () => {
        expect(game.classes.foreground).to.be.a('string');
    });
    it('expects the type of game.classes.hemisphere to be a string', () => {
        expect(game.classes.hemisphere).to.be.a('string');
    });
    it('expects the type of game.classes.layer to be a string', () => {
        expect(game.classes.layer).to.be.a('string');
    });
    it('expects the type of game.classes.tile to be a string', () => {
        expect(game.classes.tile).to.be.a('string');
    });

    it('expects the type of game.sizes to be an object', () => {
        expect(game.sizes).to.be.a('object');
    });
    it('expects the type of game.sizes.tilex to be a number', () => {
        expect(game.sizes.tilex).to.be.a('number');
    });
    it('expects the type of game.sizes.tiley to be a number', () => {
        expect(game.sizes.tiley).to.be.a('number');
    });
});
