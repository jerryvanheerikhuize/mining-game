export const game = {
    viewport: 'viewport',
    classes: {
        world: 'world',
        life: 'life',
        backgroundcolor: 'backgroundcolor blur',
        background: 'background',
        walls: 'walls',
        blocks: 'blocks',
        objects: 'objects',
        player: 'player',
        foreground: 'foreground',
        hemisphere: 'hemisphere',
        layer: 'layer',
        tile: 'tile',
    },
    sizes: {
        tilex: 32,
        tiley: 32,
        viewport: {
            width: 21,
            height: 13,
        },
    },
};
