import { keyState, resetEvents, initEvents, mouseDown, keyUp, playerEventHandler, keyDown, playerInput } from './actions.js';
import { expect, assert } from 'chai';
import sinon from 'sinon';

describe('engine/input/action.js', () => {
    it('expects the type of keyState to be an object', () => {
        expect(keyState).to.be.an('object');
    });

    it('expects the type of resetEvents to be an function', () => {
        expect(resetEvents).to.be.a('function');
    });

    it('expects the type of initEvents to be an function', () => {
        expect(initEvents).to.be.a('function');
    });

    it('expects the type of mouseDown to be an function', () => {
        expect(mouseDown).to.be.a('function');
    });

    it('expects the type of keyUp to be an function', () => {
        expect(keyUp).to.be.a('function');
    });

    it('expects the type of playerEventHandler to be an function', () => {
        expect(playerEventHandler).to.be.a('function');
    });

    it('expects the type of keyDown to be an function', () => {
        expect(keyDown).to.be.a('function');
    });

    it('expects the type of playerInput to be an function', () => {
        expect(playerInput).to.be.a('function');
    });

    // Start assertions
    it('expects resetEvents to be called once', () => {
        let obj = {};
        obj.f = resetEvents;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects initEvents to be called once', () => {
        let obj = {};
        obj.f = initEvents;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects mouseDown to be called once', () => {
        let obj = {};
        obj.f = mouseDown;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects mouseDown to be called twice', () => {
        const event = {
            type: 'mousedown',
            preventDefault: function () {
                // empty
            },
        };

        let obj = {};
        obj.f = mouseDown;
        const spy = sinon.spy(obj, 'f');

        obj.f(event);

        assert(spy.callCount, 2);
    });

    it('expects keyUp to be called once', () => {
        let obj = {};
        obj.f = keyUp;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects playerEventHandler to be called once', () => {
        let obj = {};
        obj.f = playerEventHandler;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects keyDown to be called once', () => {
        let obj = {};
        obj.f = keyDown;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects playerInput to be called once', () => {
        let obj = {};
        obj.f = playerInput;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });
});
