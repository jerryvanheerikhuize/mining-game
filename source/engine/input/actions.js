import { game } from '../../configs/game.js';
import { tiles } from '../../mappers/tiles.js';
import { changeWorld } from '../../renderers/renderers.js';
import { changeLife, playerPosition } from '../../renderers/renderers.js';

// import { playerPosition } from '';

let mouseEvent, keyEvent;
let currentPos;
let tempPos;
let buildMode;
let playerMoveDir;
let playerEvent = false;
let playerEventInterval;
let playerSpeed = 250; // action interval in ms

export let keyState = {
    'moveUp': false,
    'moveRight': false,
    'moveDown': false,
    'moveLeft': false,
    'keysPressed': 0,
};

export function resetEvents() {
    buildMode = false;
    mouseEvent = false;
    keyEvent = false;
    currentPos = false;
    tempPos = false;
}

export function initEvents() {
    // console.log('init events & reset');
    resetEvents();
}

export function mouseDown(event) {
    if (event === undefined) {
        return false;
    };

    event.preventDefault();

    if (mouseEvent || event.type === 'mouseup') {
        let values;
        let world = document.getElementById('world');
        let st = window.getComputedStyle(world, null);
        let tr = st.getPropertyValue('-webkit-transform') ||
            st.getPropertyValue('-moz-transform') ||
            st.getPropertyValue('-ms-transform') ||
            st.getPropertyValue('-o-transform') ||
            st.getPropertyValue('transform') ||
            'Either no transform set, or browser doesnt do getComputedStyle';
        if (tr !== 'none') {
            values = tr.split('(')[1];
            values = values.split(')')[0];
            values = values.split(',');
            values = [values[4], values[5]];
        } else {
            values = [0, 0];
        }
        const X = Math.floor((event.pageX - viewport.offsetLeft - values[0]) / game.sizes.tilex);
        const Y = Math.floor((event.pageY - viewport.offsetTop - values[1]) / game.sizes.tiley);
        tempPos = (`_tile_${(X)}-${(Y)}`);
        currentPos = (`_tile_${(playerPosition.x)}-${(playerPosition.y)}`);
        if (tempPos !== currentPos && (X >= 0 && X <= game.sizes.viewport.width) && (Y >= 0 && Y <= game.sizes.viewport.height)) {
            currentPos = (`_tile_${(X)}-${(Y)}`);
            buildMode = changeWorld(X, Y, tiles, buildMode);
        }
    }
}
export function keyUp(event) {
    if (event === undefined) {
        return false;
    };
    // playerEvent = false;
    switch (event.code) {
    case 'ArrowUp':
        // case 'KeyW':
        // event += 1;
        keyState.keysPressed -= 1;
        keyState.moveUp = false;
        break;
    case 'ArrowRight':
        // case 'KeyD':
        keyState.keysPressed -= 1;
        keyState.moveRight = false;
        break;
    case 'ArrowDown':
        // case 'KeyS':
        keyState.keysPressed -= 1;
        keyState.moveDown = false;
        break;
    case 'ArrowLeft':
        // case 'KeyA':
        keyState.keysPressed -= 1;
        keyState.moveLeft = false;
        break;
    }

    if (keyState.keysPressed === 0) {
        playerEvent = false;
    }
}

export function playerEventHandler(action) {
    if (action === undefined) {
        return false;
    };

    if (!playerEventInterval) {
        changeLife(action, playerMoveDir[0], playerMoveDir[1]);
        playerEventInterval = setInterval(function () {
            if (playerEvent) {
                changeLife(action, playerMoveDir[0], playerMoveDir[1]);
            } else {
                // clear self if no playerEvent!
                clearInterval(playerEventInterval);
                playerEventInterval = false;
            }
        }, playerSpeed);
    }
}

export function keyDown(event) {
    if (event === undefined) {
        return false;
    };
    keyEvent = true;
    switch (event.code) {
    case 'ArrowUp':
        if (!keyState.moveUp) {
            keyState.keysPressed += 1;
        }
        keyState.moveUp = true;
        playerEvent = 'playerMove';
        playerMoveDir = [0, -1];
        break;
    case 'ArrowRight':
        if (!keyState.moveRight) {
            keyState.keysPressed += 1;
        }
        keyState.moveRight = true;
        playerEvent = 'playerMove';
        playerMoveDir = [1, 0];
        break;
    case 'ArrowDown':
        if (!keyState.moveDown) {
            keyState.keysPressed += 1;
        }
        keyState.moveDown = true;
        playerEvent = 'playerMove';
        playerMoveDir = [0, 1];
        break;
    case 'ArrowLeft':
        if (!keyState.moveLeft) {
            keyState.keysPressed += 1;
        }
        keyState.moveLeft = true;
        playerEvent = 'playerMove';
        playerMoveDir = [-1, 0];
        break;
    default:
        keyEvent = false;
        // console.log(event.code);
        break;
    }

    if (keyEvent) {
        playerEventHandler(playerEvent);
    }
}

export function playerInput(event) {
    if (event === undefined) {
        return false;
    }
    // prevents browser source dragging ( like drag image to desktop, etc. )
    // switch event
    switch (event.type) {
    case 'mouseup':
        mouseDown(event);
        resetEvents();
        break;
    case 'mousedown':
        // if(!mouseEvent) {
        mouseEvent = event.type;
        mouseDown(event);
        // }
        break;
    case 'mousemove':
        if (mouseEvent) {
            mouseDown(event);
        }
        break;
    case 'keyup':
        keyUp(event);
        break;
    case 'keydown':
        keyDown(event);
        break;
    case 'mouseout':
        mouseDown(event);
        resetEvents();
        break;
    }
}
