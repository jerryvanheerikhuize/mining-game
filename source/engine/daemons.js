import { playerInput, resetEvents } from './input/actions.js';

export function wakeDaemons() {
    resetEvents();

    window.addEventListener('mousedown', playerInput, false);
    window.addEventListener('mouseup', playerInput, false);
    window.addEventListener('mousemove', playerInput);
    window.addEventListener('keydown', playerInput);
    window.addEventListener('keyup', playerInput);
}
