import { wakeDaemons } from './daemons.js';
import sinon from 'sinon';
import { expect, assert } from 'chai';

beforeEach(() => {
    // this is a temporary solution, add a headless browser like phantomjs to simulate a browser with a DOM
    global.window = {
        addEventListener: function () {
            // empty
        },
    };
});

describe('engine/daemons.js', () => {
    // Check the types
    it('expects the type of wakeDaemons to be an function', () => {
        expect(wakeDaemons).to.be.a('function');
    });

    it('expects wakeDaemons to be called once', () => {
        let obj = {};
        obj.f = wakeDaemons;
        const spy = sinon.spy(obj, 'f');
        obj.f();

        assert(spy.callCount, 1);
    });
});
