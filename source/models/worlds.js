export const worlds = {

    'earth': {
        'upsideUp': {
            'space': {
                '_setup': {
                    'width': 60,
                    'height': 28,
                    'backgroundcolors': [{
                        'r': 69,
                        'g': 71,
                        'b': 60,
                        'a': 1,
                    },
                        // {
                        // 	'r': 56,
                        // 	'g': 42,
                        // 	'b': 118,
                        // 	'a': 1,
                        // }, {
                        // 	'r': 29,
                        // 	'g': 29,
                        // 	'b': 29,
                        // 	'a': 1,
                        // }, {
                        // 	'r': 44,
                        // 	'g': 46,
                        // 	'b': 27,
                        // 	'a': 1,
                        // }, {
                        // 	'r': 51,
                        // 	'g': 53,
                        // 	'b': 43,
                        // 	'a': 1,
                        // }, {
                        // 	'r': 7,
                        // 	'g': 18,
                        // 	'b': 15,
                        // 	'a': 1,
                        // }, {
                        // 	'r': 0,
                        // 	'g': 0,
                        // 	'b': 0,
                        // 	'a': 1,
                        // },
                    ],
                },
                'backgrounds': [
                    ' ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★ ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                    '★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★',
                ],
                'walls': [
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                ],
                'blocks': [
                    '•••••~•  ~~~~                ~                              ',
                    '•••• ~ ~~~                   ~                              ',
                    '•••  ~~~   ••••              ~                              ',
                    '• •  ~    ••••••••••        ~~                              ',
                    '    ~~    ••••••••••      ~~                                ',
                    '    ~       ••••••        ~                  •••            ',
                    '    ~                   ~~~                 •••••••         ',
                    '~~~~~~~~~~~~~~~~~~~~~~~~~                     ••••          ',
                    ' ** *  *                                                    ',
                    '  ** *   ••• •   • • ••  ••    ••• ••• ••  ••  • •          ',
                    ' *****   •   •   • • • • • •     • •   • • • • • •          ',
                    '  ***  * ••• •   ••• • • • •     • ••  ••  ••  •••          ',
                    '   *     •   •    •  • • • •   • • •   • • • •  •           ',
                    '         •   •••  •  • • • •    •  ••• • • • •  •           ',
                    '                                                            ',
                    '           •••••••••••••••••                                ',
                    '        ••••••••••••••••••••••                              ',
                    '          ••••••••••••••••••••                              ',
                    '              •••••••••••••                 •••             ',
                    '                                       ••••••••             ',
                    '                                         ••••               ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '       •                                                    ',
                    '      •••                                                   ',
                    '                                                            ',
                    '                                                            ',
                ],
                'objects': [
                    '                                                            ',
                    '                                                            ',
                    '                 ✗                                          ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                         ✗                                  ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                ],
                'foregrounds': [
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                    '                                                            ',
                ],
            },
            'ionosphere': {
                '_setup': {},
                'background': [],
                'blocks': [],
                'objects': [],
                'foreground': [],
            },
        },
    },

};
