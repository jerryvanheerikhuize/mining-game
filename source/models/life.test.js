import { life } from './life.js';
import { expect } from 'chai';

describe('engine/input/action.js', () => {
    it('expects the type of life to be a object', () => {
        expect(life).to.be.an('object');
    });
});
