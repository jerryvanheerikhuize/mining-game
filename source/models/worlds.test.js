import {
    worlds,
} from './worlds.js';
import {
    expect,
} from 'chai';

describe('models/worlds.js', () => {
    // Prevents additions and deletions
    it('expects the length of worlds to equal 1', () => {
        expect(Object.keys(worlds).length).to.equal(1);
    });

    it('expects the length of worlds.earth to equal 1', () => {
        expect(Object.keys(worlds.earth).length).to.equal(1);
    });

    it('expects the length of worlds.earth.upsideUp to equal 2', () => {
        expect(Object.keys(worlds.earth.upsideUp).length).to.equal(2);
    });

    it('expects the length of worlds.earth.upsideUp.space to equal 6', () => {
        expect(Object.keys(worlds.earth.upsideUp.space).length).to.equal(6);
    });

    it('expects the length of worlds.earth.upsideUp.space._setup to equal 3', () => {
        expect(Object.keys(worlds.earth.upsideUp.space._setup).length).to.equal(3);
    });

    // Check the types
    it('expects the type of worlds to be an object', () => {
        expect(worlds).to.be.a('object');
    });

    it('expects the type of worlds.earth to be an object', () => {
        expect(worlds.earth).to.be.a('object');
    });
});
