import { HBDataServer } from './HBDataServer.js';
import { expect } from 'chai';

describe('HBDataServer/HBDataServer.js', () => {
    // Check the types
    it('expects the type of HBDataServer to be a function', () => {
        expect(HBDataServer).to.be.a('function');
    });
});
