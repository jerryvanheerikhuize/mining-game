export let playerPosition = {
    'x': 0,
    'y': 0,
};

/**
 * This function adds sides to the tiles
 * @name renderTileSides
 * @param {object} tile a tile object
 * @returns nothing
 */
export function renderTileSides(tile) {
    let block = false,
        mask = '0000',
        self = document.getElementById(`_tile_${tile.x}-${tile.y}`);
    if (self) {
        // self.className = 'tile solid';
        if (self.classList.contains('solid')) {
            block = 'solid';
            self.className = 'tile solid';
        } else if (self.classList.contains('liquid')) {
            block = 'liquid';
            self.className = 'tile liquid';
        } else if (self.classList.contains('destructible')) {
            self.className = 'tile destructible';
        } else {
            self.className = 'tile ';
        }

        if (self.className === 'tile solid' || self.className === 'tile liquid') {
            mask = '';
            // top
            let neighbor = document.getElementById(`_tile_${(tile.x + 0)}-${(tile.y + -1)}`);
            if (null !== neighbor && neighbor.classList.contains(block)) {
                self.className += ' t1';
                mask += '1';
            } else {
                self.className += ' t0';
                mask += '0';
            }
            // right
            neighbor = document.getElementById(`_tile_${(tile.x + 1)}-${(tile.y + 0)}`);
            if (null !== neighbor && neighbor.classList.contains(block)) {
                self.className += ' r1';
                mask += '1';
            } else {
                self.className += ' r0';
                mask += '0';
            }

            // bottom
            neighbor = document.getElementById(`_tile_${(tile.x + 0)}-${(tile.y + 1)}`);
            if (null !== neighbor && neighbor.classList.contains(block)) {
                self.className += ' b1';
                mask += '1';
            } else {
                self.className += ' b0';
                mask += '0';
            }

            // left
            neighbor = document.getElementById(`_tile_${(tile.x + -1)}-${(tile.y + 0)}`);
            if (null !== neighbor && neighbor.classList.contains(block)) {
                self.className += ' l1';
                mask += '1';
            } else {
                self.className += ' l0';
                mask += '0';
            }
            self.className += ` mask-${mask}`;
        }
    } else {
        // console.log(`non existing tile: ${(tile.x)} ${(tile.y)}`);
    }
}

export function renderBackgroundColor(tile, backgroundcolor, parentDOM) {
    const elDOM = document.createElement('div');
    const colorStr = `rgba(${backgroundcolor.r},${backgroundcolor.g},${backgroundcolor.b},${backgroundcolor.a});`;
    elDOM.setAttribute('class', 'tile');
    elDOM.setAttribute('style', `background-color: ${colorStr}`);

    if (parentDOM) {
        parentDOM.appendChild(elDOM);
    } else {
        document.appendChild(elDOM);
    }
}

export function renderTileRandomizer(tile, tiles, block, x = 0, rand = 0) {
    if (tile === undefined) {
        return;
    }
    // let temp;
    let depth = x;
    depth += 1;
    let rando;
    // let blockNum;
    let t = tiles[block];
    let prio = t.randomizer[x];
    if (prio) {
        // let r = Math.abs(prio);
        if (!rand) {
            rando = Math.floor(Math.random() * 1000);
        } else {
            rando = rand;
        }
        if (rando >= t.randomizer[x]) {
            return depth;
        }
        let rtrTemp = renderTileRandomizer(tile, tiles, block, x + 1, rando);
        return rtrTemp;
    }
    return x + 1;
}

export function renderBackgroundTile(tile, bg, parentDOM, tiles) {
    if (tile === undefined) {
        return;
    }
    let tileType = tiles[tile.block].type;
    //
    let background = ' ';
    const elDOM = document.createElement('div');
    elDOM.setAttribute('class', 'tile');

    if (tileType === 'empty') {
        let rTR = renderTileRandomizer(tile, tiles, background);
        const src = tiles[background].path + rTR + tiles[background].extension;

        const elFigureDOM = document.createElement('figure');
        elFigureDOM.setAttribute('style', `background-image:url("${src}");`);
        elDOM.appendChild(elFigureDOM);
    }

    parentDOM.appendChild(elDOM);
    // }
}

export function renderBlockTile(tile, block, parentDOM, tiles, buildMode = false) {
    let tileDepth, rTR, tileType, elDOM, elImgDOM, src, struct, changed = false;
    let tileClass = 'tile';
    elDOM = document.getElementById(`_tile_${tile.x}-${tile.y}`);

    tileType = tiles[block].type;
    if (!elDOM) {
        // CREATE block during init
        struct = 'create';
        elDOM = document.createElement('div');
        elDOM.setAttribute('id', `_tile_${tile.x}-${tile.y}`);
        if (tileType === 'solid') {
            tileDepth = Math.floor(Math.random() * 5)* 5;
            elDOM.setAttribute('style', `filter:brightness(${50 + tileDepth *3 }%); transform:translateY(${-tileDepth}px);`);
        }

        elDOM.setAttribute('data-access', tiles[block].access);
        rTR = renderTileRandomizer(tile, tiles, block);
        src = tiles[block].path + rTR + tiles[block].extension;
        elImgDOM = document.createElement('div');
        elImgDOM.setAttribute('style', `background-image:url("${src}");`);

        // add backdrop tile
        let tileBack = document.createElement('div');
        tileBack.setAttribute('class', 'backdrop');
        elDOM.appendChild(tileBack);

        // add mask tile
        let tileMask = document.createElement('div');
        tileMask.setAttribute('class', 'mask');
        // add above tile
        let tileAbove = document.createElement('div');
        tileAbove.setAttribute('class', 'above');
        tileMask.appendChild(tileAbove);

        // add ground tile
        elImgDOM.setAttribute('class', 'ground');
        tileMask.appendChild(elImgDOM);
        // add below tile
        let tileBelow = document.createElement('div');
        tileBelow.setAttribute('class', 'below');
        tileMask.appendChild(tileBelow);
        elDOM.appendChild(tileMask);

        // }
        parentDOM.appendChild(elDOM);
        if (tile.block !== ' ') {
            if (!elDOM.classList.contains(tileType)) {
                tileClass += ` ${tileType}`;
            }
        }
    } else {
        // UPDATE block runtime
        elImgDOM = elDOM.getElementsByTagName('figure');
        struct = buildMode;

        if (elDOM.classList.contains('liquid') || elDOM.classList.contains('solid') || elDOM.classList.contains('destructible')) {
            if (!struct) {
                struct = 'destruct';
            }
        } else {
            if (!struct) {
                struct = 'construct';
            }
        }
        if (!struct || struct === 'construct') {
            const newBlock = '•';
            rTR = renderTileRandomizer(tile, tiles, newBlock);
            src = tiles[newBlock].path + rTR + tiles[newBlock].extension;
            elDOM.setAttribute('data-access', tiles[newBlock].access);
            elImgDOM[0].setAttribute('style', `background-image:url("${src}");`);
            tileClass += ' solid';
        } else if (struct === 'destruct') {
            struct = 'destruct';
            const newBlock = ' ';
            rTR = renderTileRandomizer(tile, tiles, newBlock);
            src = tiles[newBlock].path + rTR + tiles[newBlock].extension;
            elDOM.setAttribute('data-access', tiles[newBlock].access);
            elImgDOM[0].setAttribute('style', `background-image:url("${src}");`);
        } else {
            tileClass = elDOM.className;
        }
    }

    changed = true;
    if (struct) {
        elDOM.setAttribute('class', tileClass);
    }
    return [struct, changed];
}

export function renderTile(tile, tiles) {
    if (tile === undefined) {
        return false;
    }

    renderBackgroundColor(tile, tile.backgroundcolor, tile.DOMReferences.backgroundcolor);
    // renderBackgroundTile(tile, tile.background, tile.DOMReferences.background, tiles);
    renderBlockTile(tile, tile.block, tile.DOMReferences.block, tiles);
}

export function renderLayer(layerStr, layerObj, parentDOM, config, tiles) {
    let row, col, tile;
    let rowBackgroundArr, rowWallArr, rowBlockArr, rowObjectArr, rowForegroundArr;
    let bgNum;

    const elDOM = document.createElement('div');
    const elBackgroundColorDOM = document.createElement('div');
    const elBackgroundDOM = document.createElement('div');
    const elWallDOM = document.createElement('div');
    const elBlockDOM = document.createElement('div');
    const elObjectDOM = document.createElement('div');
    const elForegroundDOM = document.createElement('div');

    const width = layerObj._setup.width * config.sizes.tilex;
    const height = layerObj._setup.height * config.sizes.tiley;

    elDOM.setAttribute('class', `${config.classes.layer} ${layerStr}`);
    elDOM.setAttribute('style', `width: ${width}px; height: ${height}px;`);

    parentDOM.appendChild(elDOM);

    elBackgroundColorDOM.setAttribute('class', `zlayer ${config.classes.backgroundcolor}`);
    elBackgroundDOM.setAttribute('class', `zlayer ${config.classes.background}`);
    elWallDOM.setAttribute('class', `zlayer ${config.classes.walls}`);
    elBlockDOM.setAttribute('class', `zlayer ${config.classes.blocks}`);
    elObjectDOM.setAttribute('class', `zlayer ${config.classes.objects}`);
    elForegroundDOM.setAttribute('class', `zlayer ${config.classes.foreground}`);

    elDOM.appendChild(elBackgroundColorDOM);
    elDOM.appendChild(elBackgroundDOM);
    elDOM.appendChild(elWallDOM);
    elDOM.appendChild(elBlockDOM);
    elDOM.appendChild(elObjectDOM);
    elDOM.appendChild(elForegroundDOM);

    for (row = 0; row < layerObj._setup.height; row++) {
        rowBackgroundArr = layerObj.backgrounds[row].split('');
        rowWallArr = layerObj.walls[row].split('');
        rowBlockArr = layerObj.blocks[row].split('');
        rowObjectArr = layerObj.objects[row].split('');
        rowForegroundArr = layerObj.foregrounds[row].split('');

        for (col = 0; col < layerObj._setup.width; col++) {
            bgNum = Math.floor(Math.random() * layerObj._setup.backgroundcolors.length);

            tile = {
                DOMReferences: {
                    backgroundcolor: elBackgroundColorDOM,
                    background: elBackgroundDOM,
                    wall: elWallDOM,
                    block: elBlockDOM,
                    object: elObjectDOM,
                    foreground: elForegroundDOM,
                },
                x: col,
                y: row,
                player: false,
                bgnum: bgNum,
                backgroundcolor: layerObj._setup.backgroundcolors[bgNum],
                background: rowBackgroundArr[col],
                wall: rowWallArr[col],
                block: rowBlockArr[col],
                object: rowObjectArr[col],
                foreground: rowForegroundArr[col],
            };

            renderTile(tile, tiles);
        }
    }

    // CALCULATE SIDES during init
    for (row = 0; row < layerObj._setup.height; row++) {
        for (col = 0; col < layerObj._setup.width; col++) {
            tile = {
                x: col,
                y: row,
            };
            renderTileSides(tile);
        }
    }
}

export function renderHemisphere(hemisphereStr, hemisphereArr, parentDOM, config, tiles) {
    const elDOM = document.createElement('div');
    elDOM.setAttribute('class', `${config.classes.hemisphere} ${hemisphereStr}`);
    parentDOM.appendChild(elDOM);

    for (let layerStr in hemisphereArr) {
        let layerObj = hemisphereArr[layerStr];
        renderLayer(layerStr, layerObj, elDOM, config, tiles);
    }
}

export function renderViewport(parentDOM, config) {
    if (parentDOM === undefined) {
        return false;
    }
    parentDOM.setAttribute('style', `width: ${config.sizes.viewport.width * config.sizes.tilex}px; height:${config.sizes.viewport.height * config.sizes.tiley}px; margin-left: ${-1 * config.sizes.viewport.width * config.sizes.tilex / 2}px; margin-top: ${-1 * config.sizes.viewport.height * config.sizes.tiley / 2}px;`);
}

export function renderLife(lifeStr, lifeObj, parentDOM, config) {
    if (lifeStr === undefined) {
        return false;
    }
    let centerx = Math.floor(config.sizes.viewport.width / 2);
    let centery = Math.floor(config.sizes.viewport.height / 2);

    playerPosition.x = centerx;
    playerPosition.y = centery;

    const elDOM = document.createElement('div');

    elDOM.setAttribute('class', `${config.classes.life} ${lifeStr}`);
    elDOM.setAttribute('id', `${lifeStr}`);
    elDOM.setAttribute('style', `left: ${centerx * config.sizes.tilex}px; top: ${centery * config.sizes.tiley}px;`);
    parentDOM.appendChild(elDOM);
}

export function renderWorld(worldStr, worldObj, parentDOM, config, tiles) {
    if (worldStr === undefined) {
        return false;
    }
    const elDOM = document.createElement('div');

    elDOM.setAttribute('class', `${config.classes.world} ${worldStr}`);
    elDOM.setAttribute('id', `${config.classes.world}`);
    parentDOM.appendChild(elDOM);

    for (let hemisphereStr in worldObj) {
        renderHemisphere(hemisphereStr, worldObj[hemisphereStr], elDOM, config, tiles);
    }
}

export function accessTile(X, Y) {
    if (X === undefined) {
        return false;
    }

    let x = playerPosition.x + X;
    let y = playerPosition.y + Y;

    let elDOM = document.getElementById(`_tile_${x}-${y}`);
    return parseInt(elDOM.getAttribute('data-access'));
}
// change world
export function changeTile(buildMode, tiles, col, row) {
    if (buildMode === undefined) {
        return false;
    }

    let X, Y, tile, buildState;
    tile = {
        x: col,
        y: row,
        player: false,
        bgnum: 1,
        block: '•',
    };

    buildState = renderBlockTile(tile, '•', '', tiles, buildMode);
    if (tile === undefined) {
        return false;
    }

    // check if tile 'solid' changed
    if (buildState[1]) {
        for (X = 0; X < 3; X++) {
            for (Y = 0; Y < 3; Y++) {
                tile = {
                    x: col + Y - 1,
                    y: row + X - 1,
                };
                renderTileSides(tile);
            }
        }
    }
    return buildState[0];
}
export function changePlayer(X, Y) {
    if (X === undefined || Y === undefined) {
        return false;
    }

    let player = document.getElementById('player');
    let dirClass = `dir${X}${Y}`;
    player.setAttribute('class', `life player ${dirClass}`);
    playerPosition.x += X;
    playerPosition.y += Y;
    let ts = 32;
    let world = document.getElementById('world');
    const m = new WebKitCSSMatrix(window.getComputedStyle(world).webkitTransform);
    const left = world.offsetLeft + m.m41;
    const top = world.offsetTop + m.m42;
    world.setAttribute('style', `transform: translate(${ (left - ( X * ts ) ) }px, ${ (top - ( Y * ts ) ) }px);`);
}

export function changeWorld(X, Y, tiles, buildMode) {
    return changeTile(buildMode, tiles, X, Y);
}

export function changeLife(type, X, Y) {
    if (X === undefined || Y === undefined) {
        return false;
    }

    let access = accessTile(X, Y);
    if (access !== 0) {
        return changePlayer(X, Y);
    }
    // console.log('not allowed to access tile!');
}
