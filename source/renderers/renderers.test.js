import { playerPosition, renderTileSides, renderBackgroundColor, renderTileRandomizer, renderBackgroundTile, renderBlockTile, renderTile, renderLayer, renderHemisphere, renderViewport, renderLife, renderWorld, accessTile, changeTile, changePlayer, changeWorld, changeLife } from './renderers.js';
import sinon from 'sinon';
import { expect, assert } from 'chai';

beforeEach(() => {
    // this is a temporary solution, add a headless browser like phantomjs to simulate a browser with a DOM
    global.document = {
        getElementById: function () {
            // empty
        },
        createElement: function () {
            return {
                setAttribute: function () {
                    // empty
                },
                setAttribute: function () {
                    // empty
                },
            };
        },
        appendChild: function () {
            // empty
        },
    };
});

const tile = { x:1, y:1 };
const backgroundcolor = { r: 255, g: 0, b: 0, a: 1 };

describe('renderers/renderers.js', () => {
    // playerPosition
    it('expects the playerPosition to be an object', () => {
        expect(playerPosition).to.be.an('object');
    });

    it('expects the playerPosition.x to be a number', () => {
        expect(playerPosition.x).to.be.a('number');
    });

    it('expects the playerPosition.y to be a number', () => {
        expect(playerPosition.y).to.be.a('number');
    });

    // renderTileSides
    it('expects the renderTileSides to be a function', () => {
        expect(renderTileSides).to.be.a('function');
    });

    it('expects renderTileSides to be called once', () => {
        let obj = {};
        obj.f = renderTileSides;
        const spy = sinon.spy(obj, 'f');
        obj.f(tile);

        assert(spy.callCount, 1);
    });

    // renderBackgroundColor
    it('expects the renderBackgroundColor to be a function', () => {
        expect(renderBackgroundColor).to.be.a('function');
    });

    it('expects renderBackgroundColor to be called once', () => {
        let obj = {};
        obj.f = renderBackgroundColor;
        const spy = sinon.spy(obj, 'f');
        obj.f(tile, backgroundcolor);

        assert(spy.callCount, 1);
    });

    // renderTileRandomizer
    it('expects the renderTileRandomizer to be a function', () => {
        expect(renderTileRandomizer).to.be.a('function');
    });

    it('expects the renderBackgroundTile to be a function', () => {
        expect(renderBackgroundTile).to.be.a('function');
    });

    it('expects the renderBlockTile to be a function', () => {
        expect(renderBlockTile).to.be.a('function');
    });

    it('expects the renderTile to be a function', () => {
        expect(renderTile).to.be.a('function');
    });

    it('expects the renderLayer to be a function', () => {
        expect(renderLayer).to.be.a('function');
    });

    it('expects the renderHemisphere to be a function', () => {
        expect(renderHemisphere).to.be.a('function');
    });

    it('expects the renderViewport to be a function', () => {
        expect(renderViewport).to.be.a('function');
    });

    it('expects the renderWorld to be a function', () => {
        expect(renderWorld).to.be.a('function');
    });

    it('expects the accessTile to be a function', () => {
        expect(accessTile).to.be.a('function');
    });

    it('expects the changeTile to be a function', () => {
        expect(changeTile).to.be.a('function');
    });

    it('expects the changePlayer to be a function', () => {
        expect(changePlayer).to.be.a('function');
    });

    it('expects the changeWorld to be a function', () => {
        expect(changeWorld).to.be.a('function');
    });

    it('expects the changeLife to be a function', () => {
        expect(changeLife).to.be.a('function');
    });

    // Start assertions
    it('expects renderTileRandomizer to be called once', () => {
        let obj = {};
        obj.f = renderTileRandomizer;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects renderBackgroundTile to be called once', () => {
        let obj = {};
        obj.f = renderBackgroundTile;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects renderTile to be called once', () => {
        let obj = {};
        obj.f = renderTile;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects renderViewport to be called once', () => {
        let obj = {};
        obj.f = renderViewport;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects renderLife to be called once', () => {
        let obj = {};
        obj.f = renderLife;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects renderWorld to be called once', () => {
        let obj = {};
        obj.f = renderWorld;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects accessTile to be called once', () => {
        let obj = {};
        obj.f = accessTile;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects changeTile to be called once', () => {
        let obj = {};
        obj.f = changeTile;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects changePlayer to be called once', () => {
        let obj = {};
        obj.f = changePlayer;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects changeWorld to be called once', () => {
        let obj = {};
        obj.f = changeWorld;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });

    it('expects changeLife to be called once', () => {
        let obj = {};
        obj.f = changeLife;
        const spy = sinon.spy(obj, 'f');

        obj.f();

        assert(spy.callCount, 1);
    });
});
