import { tiles } from './tiles.js';
import { expect } from 'chai';

describe('mappers/tiles.js', () => {
    // Prevents additions and deletions
    it('expects the length of tiles to equal 5', () => {
        expect(Object.keys(tiles).length).to.equal(5);
    });

    it('expects the length of tiles[\' \'] to equal 6', () => {
        expect(Object.keys(tiles[' ']).length).to.equal(6);
    });

    it('expects the length of tiles[\'★\'] to equal 6', () => {
        expect(Object.keys(tiles['★']).length).to.equal(6);
    });

    it('expects the length of tiles[\'•\'] to equal 6', () => {
        expect(Object.keys(tiles['•']).length).to.equal(6);
    });

    it('expects the length of tiles[\'*\'] to equal 6', () => {
        expect(Object.keys(tiles['*']).length).to.equal(6);
    });

    it('expects the length of tiles[\'~\'] to equal 6', () => {
        expect(Object.keys(tiles['~']).length).to.equal(6);
    });

    // Check the types
    it('expects the type of tiles[\' \'] to be an object', () => {
        expect(tiles[' ']).to.be.a('object');
    });

    it('expects the type of tiles[\'★\'] to be an object', () => {
        expect(tiles['★']).to.be.a('object');
    });

    it('expects the type of tiles[\'•\'] to be an object', () => {
        expect(tiles['•']).to.be.a('object');
    });

    it('expects the type of tiles[\'*\'] to be an object', () => {
        expect(tiles['*']).to.be.a('object');
    });

    it('expects the type of tiles[\'~\'] to be an object', () => {
        expect(tiles['~']).to.be.a('object');
    });
});
