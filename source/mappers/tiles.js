/*

*/
export const tiles = {
    ' ': {
        path: '/_resources/imgs/tiles/world/concept/blocks/plain/',
        tileCount: 4,
        extension: '.png',
        type: 'empty',
        randomizer: [75, 50, 25, -1],
        access: 1,
    },
    '★': {
        path: '/_resources/imgs/tiles/world/blocks/destructible/plants/',
        tileCount: 1,
        extension: '.png',
        type: 'solid',
        randomizer: [-1],
        access: 0,
    },
    '•': {
        path: '/_resources/imgs/tiles/world/concept/blocks/solid/',
        tileCount: 2,
        extension: '.png',
        type: 'solid',
        randomizer: [50, -1],
        access: 0,
    },
    '*': {
        path: '/_resources/imgs/tiles/world/blocks/destructible/plants/',
        tileCount: 1,
        extension: '.png',
        type: 'destructible',
        randomizer: [-1],
        access: 0,
    },
    '~': {
        path: '/_resources/imgs/tiles/world/blocks/liquid/water/',
        tileCount: 1,
        extension: '.png',
        type: 'liquid',
        randomizer: [-1],
        access: 0,
    },
};
