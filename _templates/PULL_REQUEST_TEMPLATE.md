<!--

Have you read our Code of Conduct? By filing a Pull Request, you are expected to comply with it, including treating everyone with respect: CODE_OF_CONDUCT.md
Do you want to ask a question? Are you looking for support? Email is the best way: hello@halfbros.com

If this pullrequest is directly related to a ticket-number, leave the title above as {ticket-number}: {title}*

-->

## What does this feature do?
<!-- 

Describe your changes in detail. 

-->

## Motivation, context and linked issue
<!-- 

Why is this change required? What problem does it solve?
If it fixes an open issue, please link to the issue here. 

-->

## How has this been tested?
<!--

Please describe in detail how you tested your changes,
so that the reviewer can follow the same path to test if it works as expected.
Please add link to where this can be tested

-->

## Types of changes
<!--

What types of changes does your code introduce? Put an `x` in all the boxes that apply.
If it's a breaking change, please make sure that everything still works

-->

[ ] Bug fix (non-breaking change which fixes an issue)

[ ] New feature (non-breaking change which adds functionality)

[ ] Breaking change (fix or feature that could cause existing functionality to not work as expected)


## Checklist:
<!-- 

Go over all the following points, and put an `x` in all the boxes that apply.
If you're unsure about any of these, don't hesitate to ask. We're in this boat together!

-->

[ ] I have tested if everything works as expected if this is a breaking change.

[ ] My code follows the javascript code style of this project, `npm run eslint` runs without errors/warnings.

[ ] My code follows the sass code style of this project, `npm run sasslint` runs without errors/warnings.

[ ] I have checked if all tests run without problems `npm test`.

[ ] I have updated the documentation accordingly if neccecary.

## Commits
<!-- 

Add the list of commits

-->
