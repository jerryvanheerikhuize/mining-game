[![Flynn & Jerry](https://imgur.com/9rmWH8u)](http://game.jvanheerikhuize.nl)

##  Games by Flynn, Jerry & Martijn, they are the best!

- [Introduction](#introduction)
- [Features](#features)
- [Feedback](#feedback)
- [Contributors](#contributors)
- [Build Process](#development-process)
- [Build Process](#build-process)

## Introduction

Flynn, Jerry & Martijn love playing and making games. This is our first project. 

## Features

A few of the things you can do with our game:

* Unlimited new worlds
* Mine mine mine
* Craft craft craft
* Build build build
* Mod mod mod

## Feedback

Feel free to send us feedback on [Email](jvanheerikhuize@gmail.com) or file an issue. Feature requests are always welcome. If you wish to contribute, please contact me.

## Contributors

* Create a feature branch
* Write your code
* Commit regularly (and add ticketnumber to commit message)
* Merge develop in your branch
* Resolve merge conflicts
* Make sure eslint passes 100%
* Make sure unit tests pass 100%
* Create pull request to merge in 'develop'
* Add reviewers to your pull request
* Merge when you have 2 appprovals (yours included)


## Development Process

### Add domain to your hosts file

* open terminal
* `cd /etc`
* `sudo nano hosts`
* enter password
* add `127.0.0.1 mg.dev` to the bottom of the file
* <ctrl> o
* <enter>
* <ctrl> x

### Run the de server
* open terminal
* go to the root folder
* `npm run serve`
* open a browser window
* navigate to `http://mg.dev:3142`

### NPM Scripts
#### ESlint your code
npm run eslint

#### Sasslint your code
npm run sasslint

#### Run unit tests
npm run test

#### Start dev server   
npm run serve


## Build Process
Will be available soon
