#! /usr/bin/env node

const fs = require('fs');
const path = require('path');

let count = 0;

if (process.argv.length <= 2){
    console.log(`Usage: ${__filename} path/to/dir`);
    process.exit(-1);
}

function searchDir(hayStack, needle, callBack) {
    if (!fs.existsSync(hayStack)) {
        console.log('\x1b[31m%s\x1b[0m', `ENOENT entry '${hayStack}' not found`);
        process.exit(-1);
    }

    let items = [];

    try {
        items = fs.readdirSync(hayStack);
    } catch (e) {
        console.log('\x1b[31m%s\x1b[0m', `error searching: ${hayStack}`);
    }

    for (let i = 0; i < items.length; i += 1) {
        const fn = path.join(hayStack, items[i]);
        const f = fs.lstatSync(fn);

        if (needle.test(fn)) {
            callBack(fn);
        } else if (f.isDirectory()) {
            searchDir(fn, needle, callBack);
        }
    }
}

console.log ('\x1b[2m%s\x1b[0m', '•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••');
console.log ('');
console.log (`\tCOUNTFILES in '${process.argv[2]}'`);
console.log ('');
console.log ('\x1b[2m%s\x1b[0m', '-------------------------------------------------------------');
console.log ('');

searchDir(process.argv[2], /^.*\.(js|scss|json)$/, (filename) => {
    count += 1;
    console.log('\x1b[32m%s\x1b[0m', `${count}\t ${filename}`);
});

console.log ('');
console.log ('\x1b[2m%s\x1b[0m', '-------------------------------------------------------------');
console.log ('');
console.log (`\t${count} files found`);
console.log ('');
console.log ('\x1b[2m%s\x1b[0m', '•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••');