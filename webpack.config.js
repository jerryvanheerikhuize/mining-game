const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        app: ['./source/entry.js', './source/sass/entry.scss'],
    },

    output: {
        path: `${__dirname}/_build/`,
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    'presets': ['es2015', 'stage-0'],
                },
            }, { // regular css files
                test: /\.css$/,
                exclude: /node_modules/,
                loader: ExtractTextPlugin.extract({
                    use: 'css-loader?importLoaders=1',
                }),
            }, { // sass / scss loader for webpack
                test: /\.(sass|scss)$/,
                exclude: /node_modules/,
                loader: ExtractTextPlugin.extract({
                    use: ['css-loader', 'sass-loader'],
                }),
            }, {
                test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
            }, {
                test: /\.(html)$/,
                exclude: /node_modules/,
                loader: 'html-loader',
            },
        ],
    },
    plugins: [
        new ExtractTextPlugin({ // define where to save the file
            filename: 'bundle.css',
            allChunks: true,
        }),
    ],
    devServer: {
        compress: true,
        disableHostCheck: true,
        port: 3142,
    },
    node: {
        // 'HB_FINDME BUG FS : this is a nasty fucker with node FS https://github.com/webpack-contrib/css-loader/issues/447',
        fs: 'empty',
    },
};
